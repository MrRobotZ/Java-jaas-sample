DROP DATABASE IF EXISTS AUTHDB;

CREATE DATABASE AUTHDB
  CHARSET = 'utf8'
  COLLATE = 'utf8_general_ci';


CREATE TABLE AUTHDB.USERS
(
  ID          	BIGINT UNSIGNED         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  USER_NAME     VARCHAR(128)            NOT NULL UNIQUE,
  PASSWORD 		VARCHAR(20)             NOT NULL,
  ROLE      	VARCHAR(10)   NOT NULL
);

USE AUTHDB;

INSERT INTO USERS(USER_NAME, PASSWORD, ROLE) VALUES
("Zaher", "pass1", "admin"),
("Salah", "pass1", "admin"),
("Zaher2", "pass2", "guest"),
("Salah2", "pass2", "user");
