package org.wildfly.swarm.examples.jaas.basic;


import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;


@Path("/admin")
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
public class EmployeeAdmin extends EmployeeResource{

    @GET
    public Response getAllEmployees() {
        return Response.ok().entity(super.get()).build();
    }

    @POST
    @Path("/add")
    @Transactional
    public Response addEmployee(Employee e){
        return Response.ok().entity(super.add(e)).build();
    }

    @DELETE
    @Path("/delete/{eID}")
    @Transactional
    public Response deleteEmployee(@PathParam("eID") int id){
        super.delete(id);
        return Response.ok().build();
    }

}
