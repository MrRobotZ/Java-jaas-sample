package org.wildfly.swarm.examples.jaas.basic;


import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/user")
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
public class EmployeeUser extends EmployeeResource {

    @GET
    public Response getAllEmployees() {
        return Response.ok().entity(super.get()).build();
    }

    @POST
    @Path("/add")
    @Transactional
    public Response addEmployee(Employee e){
        return Response.ok().entity(super.add(e)).build();
    }

}
