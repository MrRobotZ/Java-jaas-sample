package org.wildfly.swarm.examples.jaas.basic;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

/**
 * @author Ken Finnigan
 */


public class EmployeeResource {

    @PersistenceContext
    private EntityManager em;

    @Context
    private SecurityContext securityContext;

    public Employee[] get() {
        return em.createNamedQuery("Employee.findAll", Employee.class).getResultList().toArray(new Employee[0]);
    }

    public Employee add(Employee e){
        return em.merge(e);
    }

    public void delete(int id){
        Employee temp = em.find(Employee.class,id);

        em.remove(temp);
    }
}
